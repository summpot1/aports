# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bmake
pkgver=20240309
pkgrel=0
pkgdesc="Portable version of the NetBSD make build tool"
url="https://www.crufty.net/help/sjg/bmake.html"
arch="all"
license="BSD-2-Clause"
subpackages="$pkgname-doc"
checkdepends="tzdata"
source="https://www.crufty.net/ftp/pub/sjg/bmake-$pkgver.tar.gz
	separate-tests.patch
	"
builddir="$srcdir/$pkgname"

# Reset MAKEFLAGS since it might contain options not supported
# by bmake. This is, for instance, the case on the builders.
export MAKEFLAGS="-j${JOBS:-1}"

prepare() {
	default_prepare

	# FIXME: Disable tests failing on musl.
	sed -i unit-tests/Makefile \
		-e "/deptgt-delete_on_error/d"
}

build() {
	sh ./boot-strap --with-default-sys-path=/usr/share/mk op=build
}

check() {
	sh ./boot-strap op=test
}

package() {
	sh ./boot-strap --prefix=/usr --with-mksrc=/usr/share/mk \
			--install-destdir="$pkgdir" op=install

	rm -rf "$pkgdir"/usr/share/man/cat1
	install -Dm644 bmake.1 \
		"$pkgdir"/usr/share/man/man1/bmake.1

	install -Dm644 README ChangeLog \
		-t "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
d48b734b3b10140ed6c4b55da29fefeaed09edd10bbea9aeb0f18ceba96ead87c6a21e44d74d4ba9f6923cd0ccd61090e63e4189b545d956efe9180c2e16a21a  bmake-20240309.tar.gz
320a0175107ed683ef1772a6884be3e0c503ec4d46b0901ae8a9e8cec4c4a5236bec934d9f451142e56addd9e2fe2ea1bc4712b7304570141230e0e1997e7a65  separate-tests.patch
"
